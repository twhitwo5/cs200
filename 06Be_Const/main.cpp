#include <iostream>
using namespace std;

#include "functions.hpp"

int main()
{
    const int MAX_PETS = 10;
    int petCount = 0;
    string pets[MAX_PETS];

    int choice;
    bool done = false;
    while ( !done )
    {
        DisplayMainMenu( petCount );
        choice = GetChoice( 1, 4 );

        if ( choice == 1 )
        {

        }
        else if ( choice == 2 )
        {

        }
        else if ( choice == 3 )
        {

        }
        else if ( choice == 4 )
        {
            done = true;
        }
    }

    InitializePets( pets, MAX_PETS, petCount );

    cout << endl << "Pet count is now " << petCount << endl << endl;

    DisplayPets( pets, petCount );

    return 0;
}
