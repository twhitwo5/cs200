#include "tests.hpp"

#include <iostream>
#include <fstream>      // File input/output
using namespace std;

void RunTests()
{
    ofstream output( "tests.txt" );
    Test_PercentToDecimal( output );
    Test_CalculateTotalPrice( output );
    Test_PricePlusTax( output );
    Test_CountChange( output );
    cout << "Test results outputted to \"tests.txt\"" << endl;
}

void Test_PercentToDecimal( ofstream& output )
{
    output << endl
        << "------------------------------------------------"
        << endl << "TEST: PercentToDecimal" << endl;
    float input;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    input = 50;
    expectedOutput = 0.50;
    actualOutput = PercentToDecimal( input );
    output << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * percent:           " << input << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    input = 75;
    expectedOutput = 0.75;
    actualOutput = PercentToDecimal( input );
    output << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * percent:           " << input << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;
}

void Test_PricePlusTax( ofstream& output )
{
    output << endl
        << "------------------------------------------------"
        << endl << "TEST: PricePlusTax" << endl;
    float inputPrice, inputTax;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    inputPrice = 1.00;
    inputTax = 0.09;
    expectedOutput = 1.09;
    actualOutput = PricePlusTax( inputPrice, inputTax );
    output << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * price:             " << inputPrice << endl;
    output << "   * tax:               " << inputTax << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    inputPrice = 100.00;
    inputTax = 0.09;
    expectedOutput = 109.00;
    actualOutput = PricePlusTax( inputPrice, inputTax );
    output << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * price:             " << inputPrice << endl;
    output << "   * tax:               " << inputTax << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;
}

void Test_CountChange( ofstream& output )
{
    output << endl
        << "------------------------------------------------"
        << endl << "TEST: CountChange" << endl;
    float inputQ, inputD, inputN, inputP;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    inputQ = 4;
    inputD = 10;
    inputN = 20;
    inputP = 100;
    expectedOutput = 4.00;
    actualOutput = CountChange( inputQ, inputD, inputN, inputP );
    output << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * quarters:          " << inputQ << endl;
    output << "   * dimes:             " << inputD << endl;
    output << "   * nickels:           " << inputN << endl;
    output << "   * pennies:           " << inputP << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    inputQ = 1;
    inputD = 1;
    inputN = 1;
    inputP = 1;
    expectedOutput = 0.41;
    actualOutput = CountChange( inputQ, inputD, inputN, inputP );
    output << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "   * quarters:          " << inputQ << endl;
    output << "   * dimes:             " << inputD << endl;
    output << "   * nickels:           " << inputN << endl;
    output << "   * pennies:           " << inputP << endl;
    output << "   * Expected output:   " << expectedOutput << endl;
    output << "   * Actual output:     " << actualOutput << endl;
}

void Test_CalculateTotalPrice( ofstream& output )
{
    output << endl
        << "------------------------------------------------"
        << endl <<  "TEST: CalculateTotalPrice" << endl;
    float inputPrices[5] = { 1.01, 2.10, 3.12, 4.15, 5.16 };
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    int inputPurchases1[] = {
        0, 0, 0, 0
    };
    int inputCount1 = 4;
    expectedOutput = 4.04;
    actualOutput = CalculateTotalPrice( inputPrices, inputPurchases1, inputCount1 );
    output << endl << "TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "* purchases[]:       0, 0, 0, 0" << endl;
    output << "* Expected output:   " << expectedOutput << endl;
    output << "* Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    int inputPurchases2[] = {
        0, 1, 2, 3, 4
    };
    int inputCount2 = 5;
    expectedOutput = 15.54;
    actualOutput = CalculateTotalPrice( inputPrices, inputPurchases2, inputCount2 );
    output << endl << "TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        output << "PASS" << endl;
    }
    else
    {
        output << "FAIL" << endl;
    }
    output << "* purchases[]:       0, 1, 2, 3, 4  " << endl;
    output << "* Expected output:   " << expectedOutput << endl;
    output << "* Actual output:     " << actualOutput << endl;
}


