#ifndef _TESTS_HPP
#define _TESTS_HPP

#include "functions.hpp"

void RunTests();
void Test_PercentToDecimal( ofstream& output );
void Test_PricePlusTax( ofstream& output );
void Test_CountChange( ofstream& output );
void Test_CalculateTotalPrice( ofstream& output );

#endif
