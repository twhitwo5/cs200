#include "tests.hpp"

#include <iostream>     // cin/cout
using namespace std;

void RunTests()
{
    Test_PercentToDecimal();
    Test_PricePlusTax();
    Test_CountChange();
}

// TODO: UNCOMMENT ME OUT ONCE YOU'VE IMPLEMENTED THE FUNCTIONS.

void Test_PercentToDecimal()
{
    /*
    cout << endl
        << "------------------------------------------------"
        << endl << "TEST: PercentToDecimal" << endl;
    float input;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    input = 50;
    expectedOutput = 0.50;
    actualOutput = PercentToDecimal( input );
    cout << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * percent:           " << input << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    input = 75;
    expectedOutput = 0.75;
    actualOutput = PercentToDecimal( input );
    cout << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * percent:           " << input << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;
    */
}

void Test_PricePlusTax()
{
    /*
    cout << endl
        << "------------------------------------------------"
        << endl << "TEST: PricePlusTax" << endl;
    float inputPrice, inputTax;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    inputPrice = 1.00;
    inputTax = 0.09;
    expectedOutput = 1.09;
    actualOutput = PricePlusTax( inputPrice, inputTax );
    cout << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * price:             " << inputPrice << endl;
    cout << "   * tax:               " << inputTax << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    inputPrice = 100.00;
    inputTax = 0.09;
    expectedOutput = 109.00;
    actualOutput = PricePlusTax( inputPrice, inputTax );
    cout << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * price:             " << inputPrice << endl;
    cout << "   * tax:               " << inputTax << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;
    */
}

void Test_CountChange()
{
    /*
    cout << endl
        << "------------------------------------------------"
        << endl << "TEST: CountChange" << endl;
    float inputQ, inputD, inputN, inputP;
    float expectedOutput;
    float actualOutput;

    // --------------------------------------------------------- TEST 1
    inputQ = 4;
    inputD = 10;
    inputN = 20;
    inputP = 100;
    expectedOutput = 4.00;
    actualOutput = CountChange( inputQ, inputD, inputN, inputP );
    cout << endl << "* TEST 1... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * quarters:          " << inputQ << endl;
    cout << "   * dimes:             " << inputD << endl;
    cout << "   * nickels:           " << inputN << endl;
    cout << "   * pennies:           " << inputP << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;

    // --------------------------------------------------------- TEST 2
    inputQ = 1;
    inputD = 1;
    inputN = 1;
    inputP = 1;
    expectedOutput = 0.41;
    actualOutput = CountChange( inputQ, inputD, inputN, inputP );
    cout << endl << "* TEST 2... ";
    if ( actualOutput == expectedOutput )
    {
        cout << "PASS" << endl;
    }
    else
    {
        cout << "FAIL" << endl;
    }
    cout << "   * quarters:          " << inputQ << endl;
    cout << "   * dimes:             " << inputD << endl;
    cout << "   * nickels:           " << inputN << endl;
    cout << "   * pennies:           " << inputP << endl;
    cout << "   * Expected output:   " << expectedOutput << endl;
    cout << "   * Actual output:     " << actualOutput << endl;
    */
}




