#include <iostream>     // Let us output to the screen
using namespace std;    // Using C++'s standard library

// Create a function that greets the user
void Hello()
{
    // Display a message
    cout << "The program works!" << endl;
    cout << "Hello, world!" << endl;
}
