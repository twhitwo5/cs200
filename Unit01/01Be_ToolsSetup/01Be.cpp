#include "01Be.hpp"             // Link to the other source file

// C++ programs start at main()
int main()
{
    // Call the Hello function
    Hello();
    
    // Exit with no errors
    return 0;
}
